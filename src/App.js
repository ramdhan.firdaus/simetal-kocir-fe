import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomeNotLoginPageComponent from './components/Not Login/HomePageComponent';
import PendaftaranTeraNotLoginComponent from './components/Not Login/PendaftaranTeraComponent';
import PengaduanNotLoginComponent from './components/Not Login/PengaduanComponent';
import WithNavNotLogin from './components/Navbar/WithNavNotLogin';
import CreateAkunComponent from './components/CreateAkunComponent';
import WithoutNav from './components/Navbar/WithoutNav'
import HomePageComponent from './components/Login/HomePageComponent';
import PengaduanComponent from './components/Login/Pengaduan/PengaduanComponent';
import TabelPengaduanComponent from './components/Login/Pengaduan/TabelPengaduanComponent'
import PendaftaranTeraComponent from './components/Login/PendaftaranTeraComponent';
import PenerbitanSPTComponent from './components/Login/SPT/PenerbitanSPTComponent';
import TambahSPTComponent from './components/Login/SPT/TambahSPTComponent';
import ReviewSPTComponent from './components/Login/SPT/ReviewSPTComponent';
import EditSPTComponent from './components/Login/SPT/EditSPTComponent';
import LayananAdminComponent from './components/Login/LayananAdminComponent';
import PelayananComponent from './components/Login/Database/Pelayanan/PelayananComponent';
import PengamatanComponent from './components/Login/Database/Pengamatan/PengamatanComponent';
import TambahDataPengamatanComponent from './components/Login/Database/Pengamatan/TambahDataPengamatanComponent';
import PengawasanComponent from './components/Login/Database/Pengawasan/PengawasanComponent';
import TambahDataPengawasanComponent from './components/Login/Database/Pengawasan/TambahDataPengawasanComponent';
import WithNavLogin from './components/Navbar/WithNavLogin';

function App() {
  return (<>
    <Router>
      <div className="App">
        <Routes>
          <Route element={<WithoutNav />}>
            <Route path="/register-akun" element={<CreateAkunComponent />}></Route>
          </Route>
          <Route element={<WithNavNotLogin />}>
            <Route path="/" element={<HomeNotLoginPageComponent />}></Route>
            <Route path="/pengaduan" element={<PengaduanNotLoginComponent />}></Route>
            <Route path="/pendaftaran-tera" element={<PendaftaranTeraNotLoginComponent />}></Route>
          </Route>
          <Route element={<WithNavLogin />}>
            <Route path="/simetal-kocir" element={<HomePageComponent />}></Route>
            <Route path="/simetal-kocir/pengaduan" element={<PengaduanComponent />}></Route>
            <Route path="/simetal-kocir/tabel-pengaduan" element={<TabelPengaduanComponent />}></Route>
            <Route path="/simetal-kocir/pendaftaran-tera" element={<PendaftaranTeraComponent />}></Route>
            <Route path="/simetal-kocir/penerbitan-spt" element={<PenerbitanSPTComponent />}></Route>
            <Route path="/simetal-kocir/penerbitan-spt/tambah-spt" element={<TambahSPTComponent />}></Route>
            <Route path="/simetal-kocir/penerbitan-spt/review/:no" element={<ReviewSPTComponent />}></Route>
            <Route path="/simetal-kocir/penerbitan-spt/edit/:no" element={<EditSPTComponent />}></Route>
            <Route path="/simetal-kocir/layanan-admin" element={<LayananAdminComponent />}></Route>
            <Route path="/simetal-kocir/database-pelayanan" element={<PelayananComponent />}></Route>
            <Route path="/simetal-kocir/database-pengamatan" element={<PengamatanComponent />}></Route>
            <Route path="/simetal-kocir/database-pengamatan/tambah-data" element={<TambahDataPengamatanComponent />}></Route>
            <Route path="/simetal-kocir/database-pengawasan" element={<PengawasanComponent />}></Route>
            <Route path="/simetal-kocir/database-pengawasan/tambah-data" element={<TambahDataPengawasanComponent />}></Route>
          </Route>
        </Routes>
      </div>
    </Router>
  </>
  );
}

export default App;

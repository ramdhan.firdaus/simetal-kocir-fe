import React, { useState } from 'react'
import axios from "axios";
import { useNavigate } from "react-router-dom";

const CreateAkunComponent = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [confPassword, setConfPassword] = useState('')
  const [msg, setMsg] = useState('')
  const [msgEmail, setMsgEmail] = useState(false)
  const [msgPass, setMsgPass] = useState(false)
  const [msgConfPass, setMsgConfPass] = useState(false)
  const history = useNavigate();

  const Register = async (e) => {
    e.preventDefault();
    try {
      await axios.post('http://localhost:5000/akun', {
        nama: name,
        role: 'PENGGUNA',
        email: email,
        username: username,
        password: password
      });
      history("/");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  }

  const validateEmail = (text) => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    setMsgEmail(!reg.test(text));

    if(reg.test(text)) {
      setEmail(text)
    }
  };

  const validatePass = (text) => {
    setMsgPass(text.length < 6);

    if(text.length >= 6) {
      setPassword(text)
    }
  };

  const validateConfPass = (text) => {
    setMsgConfPass(!(text === password));

    if(text === password) {
      setConfPassword(text)
    }
  };
  
  return (
    <section className="vh-100" style={{ backgroundColor: "#508bfc" }}>
      {(() => {
        if (msg !== '') {
          return <div className="alert alert-danger alert-dismissible fade show" role="alert">
            {msg}
            <button type="button" className="btn-close" onClick={() => setMsg('')}></button>
          </div>
        }
      })()}
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-7">
            <div className="card shadow-2-strong" style={{ borderRadius: "1rem" }}>
              <div className="card-body px-5 py-4 text-center">

                <h3 className="mb-3">Register Akun</h3>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Nama :</label>
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder='Masukkan Nama'
                      onChange={(e) => setName(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Email :</label>
                  <div className="input-group">
                    <input type="email" className="form-control" placeholder='Masukkan Email'
                      onChange={(e) => validateEmail(e.target.value)} />
                  </div>
                  {(() => {
                    if (msgEmail) {
                      return <p className='message-error'>Format Email Tidak Sesuai</p>
                    }
                  })()}
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Username :</label>
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder='Masukkan Username'
                      onChange={(e) => setUsername(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Password :</label>
                  <div className="input-group">
                    <input type="password" className="form-control" placeholder='Masukkan Password'
                      onChange={(e) => validatePass(e.target.value)} />
                  </div>
                  {(() => {
                    if (msgPass) {
                      return <p className='message-error'>Password Minimal 6 Karakter</p>
                    }
                  })()}
                </div>

                <div className="form-outline mb-3">
                  <label className="control-label d-flex p-2 ">Confirm Password :</label>
                  <div className="input-group">
                    <input type="password" className="form-control" placeholder='Masukkan Password Lagi'
                      onChange={(e) => validateConfPass(e.target.value)} />
                  </div>
                  {(() => {
                    if (msgConfPass) {
                      return <p className='message-error'>Password dan Konfirmasi Password Tidak Sesuai</p>
                    }
                  })()}
                </div>

                <button className="btn btn-primary btn-block mx-1" type="submit" onClick={() => history("/")}>Back</button>
                <button className="btn btn-primary btn-block mx-1" type="submit" onClick={Register}>Register</button>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default CreateAkunComponent
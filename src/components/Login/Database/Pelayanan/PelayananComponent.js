import React, { useEffect, useState } from 'react'
import $, { data } from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../../helpers/url';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

const PelayananComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState({});
    const history = useNavigate();
    const axiosJWT = axios.create();

    const [dataTera, setDataTera] = useState([])
    const [kajiShow, setKajiShow] = useState(false);
    const [dataUpdate, setDataUpdate] = useState({})
    const [ujiShow, setUjiShow] = useState(false);

    $(".database").addClass("active")
    $(".pelayanan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            if (users.role !== undefined) {
                if (users.role.nama === 'ADMIN' || users.role.nama === 'PENGGUNA') {
                    await axios.get(`${url}/pelayanan-tera`).then((res) => {
                        setDataTera(res.data)
                    })
                } else if (users.role.nama === 'PENERA') {
                    await axios.get(`${url}/pelayanan-tera/uji`).then((res) => {
                        setDataTera(res.data)
                    })
                } else if (users.role.nama === 'BENDAHARA PENERIMAAN') {
                    await axios.get(`${url}/pelayanan-tera/bayar`).then((res) => {
                        setDataTera(res.data)
                    })
                }
            }
        }

        getApi()
    }, [users]);

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    const terbitSKHP = (i, element, status) => {

    }

    const pembayaran = (i, element, status) => {
        const updateDataTera = [...dataTera]

        const id = element.uttpId
        let data = {
            statusPembayaran: status
        }
        axios.put(`${url}/pelayanan-tera/bayar/${id}`, data)

        element.statusPembayaran = status
        updateDataTera[i] = element
        setDataTera(updateDataTera)
    }

    const kajiUlang = (i, element) => {
        let data = {
            element: element,
            i: i
        }
        setDataUpdate(data)
        setKajiShow(true);
    }

    const closeKajiModal = () => {
        setKajiShow(false);
        setDataUpdate({})
    }

    const uji = (i, element) => {
        let data = {
            element: element,
            i: i
        }
        setDataUpdate(data)
        setUjiShow(true);
    }

    const closeUjiModal = () => {
        setUjiShow(false);
        setDataUpdate({})
    }

    const update = (status) => {
        const updateData = { ...dataUpdate }
        const updateDataTera = [...dataTera]
        let data = {
            status: status
        }
        const id = updateData.element.uttpId

        axios.put(`${url}/pelayanan-tera/${id}`, data)

        updateData.element.status = status
        updateDataTera[updateData.i] = updateData.element
        setDataTera(updateDataTera)
        closeKajiModal()
        closeUjiModal()
    }

    return (<div className='container'>
        <div>
            <Modal show={kajiShow} onHide={closeKajiModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Pengkajian Ulang</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>Bagaimana Hasil dari Pengkajian Ulangnya?</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={() => update('TOLAK PENGKAJIAN ULANG')}>
                        Tolak
                    </Button>
                    <Button variant="success" onClick={() => update('SELESAI KAJI ULANG')}>
                        Terima
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>

        <div>
            <Modal show={ujiShow} onHide={closeUjiModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Pengujian</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>Bagaimana Hasil dari Pengujianya?</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={() => update('TOLAK PENGUJIAN')}>
                        Tolak
                    </Button>
                    <Button variant="success" onClick={() => update('PENGUJIAN SELESAI')}>
                        Terima
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>

        <div className="row mt-5 mb-2">
            <div className="col-2"></div>
            <div className="col-8 text-center">
                <h5><strong>DATABASE PELAYANAN TERA/TERA ULANG</strong></h5>
                <h5>(LIST ORDER PELAYANAN)</h5>
            </div>
        </div>
        <br></br>
        <div className="row">
            <div className="col-12">
                <table className="table table-bordered align-middle">
                    <thead className='text-center'>
                        <tr>
                            <th width="10px" scope="col" rowSpan={2}>No</th>
                            <th width="100px" scope="col" rowSpan={2}>No Order</th>
                            <th width="100px" scope="col" rowSpan={2}>Pemohon</th>
                            <th width="100px" scope="col" rowSpan={2}>Alamat</th>
                            <th width="100px" scope="col" colSpan={5}>Identitas UTTP</th>
                            <th width="100px" scope="col" rowSpan={2}>Retribusi</th>
                            <th width="100px" scope="col" rowSpan={2}>Status</th>
                            <th width="200px" scope="col" rowSpan={2}>Aksi</th>
                        </tr>
                        <tr>
                            <th width="50px" scope="col">Jenis UTTP</th>
                            <th width="50px" scope="col">Kapasitas</th>
                            <th width="50px" scope="col">Merek</th>
                            <th width="50px" scope="col">Model/Type</th>
                            <th width="50px" scope="col">No Seri</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dataTera.map((element, i) => {
                            return <tr key={i}>
                                <th scope="row" className='text-center'>{i + 1}</th>
                                <td>{element.uttp.pendaftaran_tera.noOrder}</td>
                                <td>{element.uttp.pendaftaran_tera.namaPemilik}</td>
                                <td>{element.uttp.pendaftaran_tera.alamatLengkap}</td>
                                <td>{element.uttp.sub_jenis_uttp.jenis}</td>
                                <td>{element.uttp.kapasitas} {element.uttp.satuan_kapasita.nama}</td>
                                <td>{element.uttp.merek}</td>
                                <td>{element.uttp.model}</td>
                                <td>{element.uttp.noSeri}</td>
                                <td>{element.uttp.sub_jenis_uttp.retribusi}</td>
                                {(() => {
                                    if (users.length !== 0) {
                                        return <>
                                            {(() => {
                                                if (users.role.nama === "ADMIN") {
                                                    return <>
                                                        <td>
                                                            {element.status.slice(0, 1) + element.status.substr(1).toLowerCase()}.
                                                            {(() => {
                                                                if (element.status !== 'MENUNGGU KAJI ULANG') {
                                                                    if (element.statusPembayaran === "LUNAS") {
                                                                        return <> Proses pembayaran selesai</>
                                                                    } else {
                                                                        return <> Menunggu proses pembayaran</>
                                                                    }
                                                                }
                                                            })()}
                                                        </td>
                                                        <td className='text-center'>
                                                            {(() => {
                                                                if (element.status === "MENUNGGU KAJI ULANG") {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => kajiUlang(i, element)}>KAJI ULANG</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>TERBITKAN SKHP</button>
                                                                    </>
                                                                } else if (element.status === "PENGUJIAN SELESAI") {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>KAJI ULANG</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => terbitSKHP(i, element, "SKHP TELAH DITERBITKAN")}>TERBITKAN SKHP</button>
                                                                    </>
                                                                } else {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>KAJI ULANG</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>TERBITKAN SKHP</button>
                                                                    </>
                                                                }
                                                            })()}
                                                        </td>
                                                    </>
                                                }
                                            })()}
                                            {(() => {
                                                if (users.role.nama === "PENERA") {
                                                    return <>
                                                        <td>
                                                            {element.status.slice(0, 1) + element.status.substr(1).toLowerCase()}.
                                                            {(() => {
                                                                if (element.status !== 'MENUNGGU KAJI ULANG') {
                                                                    if (element.statusPembayaran === "LUNAS") {
                                                                        return <> Proses pembayaran selesai</>
                                                                    } else {
                                                                        return <> Menunggu proses pembayaran</>
                                                                    }
                                                                }
                                                            })()}
                                                        </td>
                                                        <td className='text-center'>
                                                            {(() => {
                                                                if (element.status === "SELESAI KAJI ULANG") {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => uji(i, element)}>UJI</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>BERITA ACARA</button>
                                                                    </>
                                                                } else if (element.status === "PENGUJIAN SELESAI") {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' disabled>UJI</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1'>BERITA ACARA</button>
                                                                    </>
                                                                }
                                                            })()}
                                                        </td>
                                                    </>
                                                }
                                            })()}
                                            {(() => {
                                                if (users.role.nama === "BENDAHARA PENERIMAAN") {
                                                    if (element.statusPembayaran === "LUNAS") {
                                                        return <>
                                                            <td>
                                                                {element.statusPembayaran.slice(0, 1) + element.statusPembayaran.substr(1).toLowerCase()}.
                                                            </td>
                                                            <td className='text-center'>
                                                                <button className='btn btn-primary btn-sm mx-1 my-1' disabled>BAYAR</button>
                                                                <button className='btn btn-primary btn-sm mx-1 my-1'>KWITANSI</button>
                                                            </td>
                                                        </>
                                                    } else if (element.statusPembayaran === "BELUM DIBAYAR") {
                                                        return <>
                                                            <td>
                                                                {element.statusPembayaran.slice(0, 1) + element.statusPembayaran.substr(1).toLowerCase()}.
                                                            </td>
                                                            <td className='text-center'>
                                                                <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => pembayaran(i, element, "LUNAS")}>BAYAR</button>
                                                                <button className='btn btn-primary btn-sm mx-1 my-1' disabled>KWITANSI</button>
                                                            </td>
                                                        </>
                                                    }
                                                }
                                            })()}
                                        </>
                                    }
                                })()}
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    </div >
    )
}

export default PelayananComponent
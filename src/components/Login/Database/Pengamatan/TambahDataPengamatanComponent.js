import React, { useEffect, useState } from 'react'
import $ from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../../helpers/url';

const TambahDataPengamatanComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);
    const history = useNavigate();
    const axiosJWT = axios.create();

    const [dataOrder, setDataOrder] = useState([])
    const [noOrder, setNoOrder] = useState('')
    const [tandaTera, setTandaTera] = useState('')
    const [hasil, setHasil] = useState('')

    $(".database").addClass("active")
    $(".pengamatan").addClass("active")

    useEffect(() => {
        const getApiNoOrder = async () => {
            let dataPendaftaran = []
            dataPendaftaran.push({ value: '--', label: 'Pilih No Order' })
            await axios.get(`${url}/pendaftaran-tera/not-pengamatan`).then((res) => {
                res.data.forEach(element => {
                    dataPendaftaran.push({ value: `${element.noOrder}`, label: `${element.noOrder}` })
                });
            })
            setDataOrder(dataPendaftaran)
        }

        getApiNoOrder()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    const submit = async () => {
        let data = {
            noOrder: noOrder,
            tandaTera: tandaTera,
            hasil: hasil.toUpperCase()
        }
        await axios.post(`${url}/pengamatan-tera`, data)
        history(-1)
    }

    return (<div className="container">
        <div className="row">
            <div className="col-2"></div>
            <div className="col-8 text-center">
                <h5>
                    FORM PENGISIAN PEMBUATAN DATA PENGAMATAN UTTP
                </h5>
            </div>
        </div>
        <br />
        <div className="row mb-2">
            <div className="col-lg-3 col-sm-2 col-1"></div>
            <div className="col-lg-2 col-sm-2 col-3 d-flex align-items-center">
                No Order Tera
            </div>
            <div className="col-lg-4 col-sm-6 col-7">
                <select className="form-select form-select-sm" onChange={(e) => setNoOrder(e.target.value)}>
                    {dataOrder.map((element, i) => {
                        return <option key={`daftar-${i}`} value={element.value} label={element.label}></option>
                    })}
                </select>
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-lg-3 col-sm-2 col-1"></div>
            <div className="col-lg-2 col-sm-2 col-3 d-flex align-items-center">
                Tanda Tera
            </div>
            <div className="col-lg-4 col-sm-6 col-7">
                <input type="number" className="form-control form-control-sm" onChange={(e) => setTandaTera(e.target.value)} />
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-lg-3 col-sm-2 col-1"></div>
            <div className="col-lg-2 col-sm-2 col-3 d-flex align-items-center">
                Hasil Pengawasan
            </div>
            <div className="col-lg-4 col-sm-6 col-7">
                <input type="text" className="form-control form-control-sm" onChange={(e) => setHasil(e.target.value)} />
            </div>
        </div>

        <div className="row">
            <div className="col-lg-6 col-sm-7 col-8"></div>
            <div className="col-3 text-end">
                <button onClick={() => history(-1)} className='btn btn-primary me-2'>
                    Cancel
                </button>
                <button onClick={submit} className='btn btn-primary'>
                    Kirim
                </button>
            </div>
        </div>
    </div>
    )
}

export default TambahDataPengamatanComponent
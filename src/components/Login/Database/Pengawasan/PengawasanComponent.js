import React, { useEffect, useState } from 'react'
import $ from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../../helpers/url';

const PengawasanComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);
    const history = useNavigate();
    const axiosJWT = axios.create();

    const [dataTera, setDataTera] = useState([])

    $(".database").addClass("active")
    $(".pengawasan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            await axios.get(`${url}/pengawasan-tera`).then((res) => {
                setDataTera(res.data)
            })
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    return (<div className='container'>
        <div className="row mt-5 mb-2">
            <div className="col-2"></div>
            <div className="col-8 text-center">
                <h5><strong>DATABASE PENGAWASAN UTTP SECARA UMUM</strong></h5>
            </div>
            <div className="col-2 align-self-end text-end">
                <a href='database-pengawasan/tambah-data'><button type='button' className='btn btn-primary'>Tambahkan Data</button></a>
            </div>
        </div>
        <br></br>
        <div className="row">
            <div className="col-12">
                <table className="table table-bordered align-middle">
                    <thead className='text-center'>
                        <tr>
                            <th width="10px" scope="col" rowSpan={2}>No</th>
                            <th width="100px" scope="col" rowSpan={2}>No Order</th>
                            <th width="100px" scope="col" rowSpan={2}>Pemohon</th>
                            <th width="100px" scope="col" rowSpan={2}>Alamat</th>
                            <th width="100px" scope="col" colSpan={5}>Identitas UTTP</th>
                            <th width="100px" scope="col" rowSpan={2}>Tanda Tera</th>
                            <th width="100px" scope="col" rowSpan={2}>Hasil Pengawasan</th>
                            <th width="200px" scope="col" rowSpan={2}>Aksi</th>
                        </tr>
                        <tr>
                            <th width="50px" scope="col">Jenis UTTP</th>
                            <th width="50px" scope="col">Kapasitas</th>
                            <th width="50px" scope="col">Merek</th>
                            <th width="50px" scope="col">Model/Type</th>
                            <th width="50px" scope="col">No Seri</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dataTera.map((element, i) => {
                            return <tr key={i}>
                                <th scope="row" className='text-center'>{i + 1}</th>
                                <td>{element.uttp.pendaftaran_tera.noOrder}</td>
                                <td>{element.uttp.pendaftaran_tera.namaPemilik}</td>
                                <td>{element.uttp.pendaftaran_tera.alamatLengkap}</td>
                                <td>{element.uttp.sub_jenis_uttp.jenis}</td>
                                <td>{element.uttp.kapasitas}</td>
                                <td>{element.uttp.merek}</td>
                                <td>{element.uttp.model}</td>
                                <td>{element.uttp.noSeri}</td>
                                <td>{element.tandaTera}</td>
                                <td>{element.hasilPengawasan.slice(0, 1) + element.hasilPengawasan.substr(1).toLowerCase()}</td>
                                <td className='text-center'>
                                    {(() => {
                                        if (users.length !== 0) {
                                            return <>
                                                {(() => {
                                                    if (users.role.nama === "PENGAWAS" || users.role.nama === "ADMIN" || users.role.nama === "PENGGUNA") {
                                                        return <>
                                                            <button className='btn btn-primary btn-sm mx-1 my-1' >EDIT</button>
                                                            <button className='btn btn-primary btn-sm mx-1 my-1'>DOWNLOAD BA</button>
                                                        </>
                                                    }
                                                })()}
                                            </>
                                        }
                                    })()}
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    )
}

export default PengawasanComponent
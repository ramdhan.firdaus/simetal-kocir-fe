import React, { useState, useEffect } from 'react'
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import $ from 'jquery'
import url from '../../helpers/url';

const HomePageComponent = () => {
  const [name, setName] = useState('');
  const [token, setToken] = useState('');
  const [expire, setExpire] = useState('');
  const [users, setUsers] = useState([]);
  const history = useNavigate();
  const axiosJWT = axios.create();

  $(".beranda").addClass("active")

  useEffect(() => {
    const getApi = async () => {
      try {
        const response = await axios.get(`${url}/token`);
        setToken(response.data.accessToken);
        const decoded = jwt_decode(response.data.accessToken);
        setExpire(decoded.exp);
      } catch (error) {
        if (error.response) {
          history("/");
        }
      }
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      await axiosJWT.get(`${url}/akun`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
      }).then((res) => {
        setUsers(res.data[0]);
      });
    }

    getApi()
  }, [token]);

  axiosJWT.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expire * 1000 < currentDate.getTime()) {
      const response = await axios.get(`${url}/token`);
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decoded = jwt_decode(response.data.accessToken);
      setExpire(decoded.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  });

  return (<div className="container">
    <section id="select">
      <div className='mx-4 my-2'>
        <strong>PROFIL KEMETRILOGIAN</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong>SEBARAN UTTP DI KOTA CIREBON</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong>TARIF RETRIBUSI</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong><i>Tracking</i> Order Pelayanan</strong>
        <div className="form-outline my-1 mx-4">
          <div className="input-group">
            <input type="email" className="form-control" />
            <button className="btn btn-primary btn-block mx-2" type="submit">Cari</button>
          </div>
        </div>
      </div>
      <br></br>
      <hr />
      <div className='mx-4 my-4'>
        <strong>LOKASI DAN KONTAK</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </section>
  </div>
  )
}

export default HomePageComponent
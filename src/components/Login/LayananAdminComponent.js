import React, { useState, useEffect } from 'react'
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import $, { data } from 'jquery'
import url from '../../helpers/url';

const LayananAdminComponent = () => {
  const [token, setToken] = useState('');
  const [expire, setExpire] = useState('');
  const [users, setUsers] = useState([]);
  const [daftarRole, setDaftarRole] = useState([])
  const [daftarPengguna, setDaftarPengguna] = useState([])
  const [daftarRolePengguna, setDaftarRolePengguna] = useState([])
  const [daftarNIPPengguna, setDaftarNIPPengguna] = useState([])
  const history = useNavigate();
  const axiosJWT = axios.create();

  $(".admin").addClass("active")

  useEffect(() => {
    const getApiRole = async () => {
      let data = [{ value: "", label: "Pilih Role Baru" }]
      await axios.get(`${url}/role`).then((res) => {
        res.data.forEach((element) => {
          data.push({ value: `${element.id}`, label: `${element.nama}` })
        })
      })
      setDaftarRole(data)
    }

    const getApiPengguna = async () => {
      let dataRole = []
      let dataNIP = []
      await axios.get(`${url}/akun/all`).then((res) => {
        res.data.forEach((element) => {
          dataRole.push('')
          dataNIP.push(element.nip)
        })
        setDaftarPengguna(res.data)
      })
      setDaftarRolePengguna(dataRole)
      setDaftarNIPPengguna(dataNIP)
    }

    getApiRole()
    getApiPengguna()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      try {
        const response = await axios.get(`${url}/token`);
        setToken(response.data.accessToken);
        const decoded = jwt_decode(response.data.accessToken);
        setExpire(decoded.exp);
      } catch (error) {
        if (error.response) {
          history("/");
        }
      }
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      await axiosJWT.get(`${url}/akun`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
      }).then((res) => {
        setUsers(res.data[0]);
      });
    }

    getApi()
  }, [token]);

  axiosJWT.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expire * 1000 < currentDate.getTime()) {
      const response = await axios.get(`${url}/token`);
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decoded = jwt_decode(response.data.accessToken);
      setExpire(decoded.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  });

  const changeRole = (value, i) => {
    const updateData = [...daftarRolePengguna]
    updateData[i] = value
    setDaftarRolePengguna(updateData)
  }

  const changeNIP = (value, i) => {
    const updateData = [...daftarNIPPengguna]
    updateData[i] = value
    setDaftarNIPPengguna(updateData)
  }

  const submit = async () => {
    let reqData = {
      pengguna: daftarPengguna,
      nip: daftarNIPPengguna,
      role: daftarRolePengguna
    }
    await axios.put(`${url}/akun`, reqData)
    window.location.reload()
  }

  return (<div className='container'>
    <div className="row">
      <div className="col-1"></div>
      <div className="col-10 text-center">
        <h4>Layanan Admin</h4>
      </div>
    </div>
    <br></br>
    <div className="row">
      <div className="col-1"></div>
      <div className="col-10">
        <table className="table table-bordered align-middle">
          <thead className='text-center'>
            <tr>
              <th width="50px" scope="col">No</th>
              <th width="300px" scope="col">Pengguna</th>
              <th width="300px" scope="col">Role Saat Ini</th>
              <th width="300px" scope="col">Ubah Role</th>
              <th width="300px" scope="col">NIP</th>
            </tr>
          </thead>
          <tbody>
            {daftarPengguna.map((element, i) => (
              <tr>
                <th scope="row" className='text-center'>{i + 1}</th>
                <td>{element.nama}</td>
                <td>{element.role.nama}</td>
                <td>
                  <select className="form-select form-select-sm" onChange={(e) => changeRole(e.target.value, i)}>
                    {daftarRole.map((element, i) => (
                      <option key={`role-${i}`} value={element.value} label={element.label}></option>
                    ))}
                  </select>
                </td>
                <td>
                  <input type="text" className="form-control form-control-sm" value={daftarNIPPengguna[i]} onChange={(e) => changeNIP(e.target.value, i)} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    <div className="row">
      <div className="col-1"></div>
      <div className="col-10 text-center">
        <button onClick={submit} className='btn btn-primary'>
          Simpan
        </button>
      </div>
    </div>
  </div>
  )
}

export default LayananAdminComponent
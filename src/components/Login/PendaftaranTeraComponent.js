import React, { useState, useEffect } from 'react'
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import $ from 'jquery'
import url from '../../helpers/url';
import Select from 'react-select';

const PendaftaranTeraComponent = () => {
  const [token, setToken] = useState('');
  const [expire, setExpire] = useState('');
  const [users, setUsers] = useState([]);

  const [jumlahUTTP, setJumlahUTTP] = useState(1);
  const [arrayUTTP, setArrayUTTP] = useState(['']);

  const [noOrder, setNoOrder] = useState('');
  const [nama, setNama] = useState('');
  const [jenisUsaha, setJenisUsaha] = useState('');
  const [noHP, setNoHP] = useState('');
  const [tanggal, setTanggal] = useState('');

  const [daftarKecamatan, setDaftarKecamatan] = useState([{}])

  const [kelurahan, setKelurahan] = useState('');
  const [daftarKelurahan, setDaftarKelurahan] = useState([{ value: '', label: 'Pilih Kelurahan Anda' }])

  const [alamatLengkap, setAlamatLengkap] = useState('');

  const [jenisPelayanan, setJenisPelayanan] = useState([]);
  const [teraTeraUlang, setTeraTeraUlang] = useState([]);
  const [jenisUTTP, setJenisUTTP] = useState([]);
  const [daftarjenisUTTP, setDaftarJenisUTTP] = useState([])

  const [kapasitas, setKapasitas] = useState([]);
  const [satuanKapasitas, setSatuanKapasitas] = useState([]);
  const [daftarSatuanKapasitas, setDaftarSatuanKapasitas] = useState([]);
  const [merek, setMerek] = useState([]);
  const [model, setModel] = useState([]);
  const [noSeri, setNoSeri] = useState([]);
  const [kelengkapanUTTP, setKelengkapanUTTP] = useState([]);
  const [SKHP, setSKHP] = useState([]);

  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  const history = useNavigate();
  const axiosJWT = axios.create();

  $(".pendaftaran").addClass("active")

  useEffect(() => {
    const getApiKecamatan = async () => {
      let data = [{ value: "", label: "Pilih Kecamatan Anda" }]
      await axios.get(`${url}/kecamatan`).then((res) => {
        res.data.forEach((element) => {
          data.push({ value: `${element.id}`, label: `${element.nama}` })
        })
      })

      setDaftarKecamatan(data)
    }

    const getApiSatuan = async () => {
      let data = [{ value: "", label: "Satuan" }]
      await axios.get(`${url}/satuan-kapasitas`).then((res) => {
        res.data.forEach((element) => {
          data.push({ value: `${element.id}`, label: `${element.nama}` })
        })
      })

      setDaftarSatuanKapasitas(data)
    }

    const getApiJenis = async () => {
      let data = [{ value: '', label: 'Pilih Jenis UTTP' }]
      await axios.get(`${url}/sub-jenis-uttp`).then((res) => {
        res.data.forEach((element) => {
          data.push({ value: `${element.id}`, label: `${element.jenis}` })
        })
      })

      setDaftarJenisUTTP(data)
    }

    getApiKecamatan()
    getApiSatuan()
    getApiJenis()
  }, []);

  useEffect(() => {
    const current = new Date();
    const getApi = async () => {
      while (true) {
        const angka = '000' + Math.floor(0 + Math.random() * 999)
        const angka1 = angka.substring(angka.length - 3)
        const reqData = `${current.getMonth()}-${current.getFullYear()}-${angka1}`
        let data = ''
        await axios.get(`${url}/pendaftaran-tera/${reqData}`).then((res) => {
          data = res.data
        })

        if (data === null) {
          setNoOrder(`${current.getMonth()}-${current.getFullYear()}-${angka1}`)
          setTanggal(`${current.getDate()} ${monthNames[current.getMonth()]} ${current.getFullYear()}`);
          break
        }

      }
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      try {
        const response = await axios.get(`${url}/token`);
        setToken(response.data.accessToken);
        const decoded = jwt_decode(response.data.accessToken);
        setExpire(decoded.exp);
      } catch (error) {
        if (error.response) {
          history("/");
        }
      }
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      await axiosJWT.get(`${url}/akun`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
      }).then((res) => {
        setUsers(res.data[0]);
      });
    }

    getApi()
  }, [token]);

  axiosJWT.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expire * 1000 < currentDate.getTime()) {
      const response = await axios.get(`${url}/token`);
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decoded = jwt_decode(response.data.accessToken);
      setExpire(decoded.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  });


  const kurangUTTP = () => {
    if (jumlahUTTP > 1) {
      setArrayUTTP(new Array(jumlahUTTP - 1).fill(''))
      setJumlahUTTP(jumlahUTTP - 1)
    }
  }

  const tambahUTTP = () => {
    setArrayUTTP(new Array(jumlahUTTP + 1).fill(''))
    setJumlahUTTP(jumlahUTTP + 1)
  }

  const changeKecamatan = (value) => {
    const getApi = async () => {
      let data = [{ value: "", label: "Pilih Kelurahan Anda" }]
      await axios.get(`${url}/kelurahan/${value}`).then((res) => {
        res.data.forEach((element) => {
          data.push({ value: `${element.id}`, label: `${element.nama}` })
        })
      })

      setDaftarKelurahan(data)
    }

    getApi()
  }

  const changeJenisPelayanan = (value, i) => {
    const updateJenisPelayanan = [...jenisPelayanan];
    updateJenisPelayanan[i] = value;
    setJenisPelayanan(updateJenisPelayanan);
  }

  const changeTeraTeraUlang = (value, i) => {
    const updateTeraTeraUlang = [...teraTeraUlang];
    updateTeraTeraUlang[i] = value;
    setTeraTeraUlang(updateTeraTeraUlang);
  }

  const changeJenisUTTP = (value, i) => {
    const updateJenisUTTP = [...jenisUTTP];
    updateJenisUTTP[i] = value;
    setJenisUTTP(updateJenisUTTP);
  }

  const changeKapasitas = (value, i) => {
    const updateKapasitas = [...kapasitas];
    updateKapasitas[i] = value;
    setKapasitas(updateKapasitas);
  }

  const changeSatuanKapasitas = (value, i) => {
    const updateSatuanKapasitas = [...satuanKapasitas];
    updateSatuanKapasitas[i] = value;
    setSatuanKapasitas(updateSatuanKapasitas);
  }

  const changeMerek = (value, i) => {
    const updateMerek = [...merek];
    updateMerek[i] = value;
    setMerek(updateMerek);
  }

  const changeModel = (value, i) => {
    const updateModel = [...model];
    updateModel[i] = value;
    setModel(updateModel);
  }

  const changeNoSeri = (value, i) => {
    const updateNoSeri = [...noSeri];
    updateNoSeri[i] = value;
    setNoSeri(updateNoSeri);
  }

  const changeKelengkapanUTTP = (value, i) => {
    const updateKelengkapanUTTP = [...kelengkapanUTTP];
    updateKelengkapanUTTP[i] = value;
    setKelengkapanUTTP(updateKelengkapanUTTP);
  }

  const changeSKHP = (value, i) => {
    const updateSKHP = [...SKHP];
    updateSKHP[i] = value;
    setSKHP(updateSKHP);
  }

  const submit = async () => {
    let requestPendaftaranTera = {
      noOrder: noOrder,
      nama: nama,
      kelurahan: kelurahan,
      alamatLengkap: alamatLengkap,
      jenisUsaha: jenisUsaha,
      noHP: noHP,
      tanggal: tanggal,
    }
    await axios.post(`${url}/pendaftaran-tera`, requestPendaftaranTera);

    let requestUTTP = {
      noOrder: noOrder,
      jenisPelayanan: jenisPelayanan,
      teraTeraUlang: teraTeraUlang,
      subJenisUTTP: jenisUTTP,
      kapasitas: kapasitas,
      satuanKapasitas: satuanKapasitas,
      merek: merek,
      model: model,
      noSeri: noSeri,
      kelengkapanUTTP: kelengkapanUTTP,
      SKHP: SKHP,
    }
    const uttp = await axios.post(`${url}/uttp`, requestUTTP);
    let requestPelayananTera = {
      uttpId: uttp.data.data
    }
    await axios.post(`${url}/pelayanan-tera`, requestPelayananTera);

    window.location.reload();
  }

  return (<div className="container">
    <div className="row">
      <div className="col-1"></div>
      <div className="col-10 text-center title">
        FORM PELAYANAN TERA/TERA ULANG TAHUN 2022
      </div>
    </div>
    <br />
    <div className="container">
      <div className="row">
        <div className="col-1"></div>
        <div className="col-11">
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              No Order
            </div>
            <div className="col-4">
              <input type="text" className="form-control form-control-sm" value={noOrder} disabled />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              Nama Pemilik UTTP
            </div>
            <div className="col-9">
              <input type="text" className="form-control form-control-sm" onChange={(e) => setNama(e.target.value)} />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              Alamat Nama Pemilik UTTP
            </div>
            <div className="col-lg-1 col-sm-2 col-3 d-flex align-items-center double-sub">
              Kecamatan
            </div>
            <div className="col-lg-8 col-sm-7 col-6">
              <Select
                value={daftarKecamatan.value}
                options={daftarKecamatan}
                defaultValue={{ value: '', label: "Pilih Kecamatan Anda" }}
                onChange={(e) => changeKecamatan(e.value)}
              />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 sub">
            </div>
            <div className="col-lg-1 col-sm-2 col-3 d-flex align-items-center double-sub">
              Kelurahan
            </div>
            <div className="col-lg-8 col-sm-7 col-6">
              <Select
                value={daftarKelurahan.value}
                options={daftarKelurahan}
                defaultValue={{ value: '', label: "Pilih Kelurahan Anda" }}
                onChange={(e) => setKelurahan(e.value)}
              />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 sub">
            </div>
            <div className="col-lg-1 col-sm-2 col-3 d-flex align-items-center double-sub">
              Alamat Lengkap
            </div>
            <div className="col-lg-8 col-sm-7 col-6">
              <textarea rows="3" className="form-control form-control-sm" onChange={(e) => setAlamatLengkap(e.target.value)}></textarea>
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              Jenis Usaha
            </div>
            <div className="col-9">
              <input type="text" className="form-control form-control-sm" onChange={(e) => setJenisUsaha(e.target.value)} />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              No HP
            </div>
            <div className="col-9">
              <input type="text" className="form-control form-control-sm" onChange={(e) => setNoHP(e.target.value)} />
            </div>
          </div>
          <div className="row mb-2">
            <div className="col-2 d-flex align-items-center sub-title">
              Tanggal
            </div>
            <div className="col-4">
              <input type="text" className="form-control form-control-sm" value={tanggal} disabled />
            </div>
          </div>
        </div>
      </div>
    </div>

    <br />

    {arrayUTTP.map((elemen, i) => (
      <div key={`uttp-${i}`} >
        <hr></hr>
        <div className="row uttp text-center mb-2">
          <div className="col-1"></div>
          <div className="col-lg-1 title-uttp">
            UTTP
          </div>
          <div className="col-lg-9">
            <div className='row'>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Jenis Pelayanan
                <input type="text" className="form-control form-control-sm" onChange={(e) => changeJenisPelayanan(e.target.value, i)} />
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Tera/Tera Ulang
                <select className="form-select form-select-sm" onChange={(e) => changeTeraTeraUlang(e.target.value, i)}>
                  <option value="" label="Pilih Tera atau Tera Ulang"></option>
                  <option value="Tera" label="Tera"></option>
                  <option value="Tera Ulang" label="Tera Ulang"></option>
                </select>
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Jenis UTTP
                <select className="form-select form-select-sm" onChange={(e) => changeJenisUTTP(e.target.value, i)}>
                  {daftarjenisUTTP.map((element, i) => {
                    return <option key={`jenis-${i}`} value={element.value} label={element.label}></option>
                  })}
                </select>
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Kapasitas
                <div className="input-group">
                  <input type="number" className="form-control form-control-sm nilai" onChange={(e) => changeKapasitas(e.target.value, i)} />
                  <select className="form-select form-select-sm satuan" onChange={(e) => changeSatuanKapasitas(e.target.value, i)}>
                    {daftarSatuanKapasitas.map((element, i) => {
                      return <option key={`satuan-${i}`} value={element.value} label={element.label}></option>
                    })}
                  </select>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Merek
                <input type="text" className="form-control form-control-sm" onChange={(e) => changeMerek(e.target.value, i)} />
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Model
                <input type="text" className="form-control form-control-sm" onChange={(e) => changeModel(e.target.value, i)} />
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                No Seri
                <input type="text" className="form-control form-control-sm" onChange={(e) => changeNoSeri(e.target.value, i)} />
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                Kelengkapan UTTP
                <textarea rows="1" className="form-control form-control-sm" onChange={(e) => changeKelengkapanUTTP(e.target.value, i)}></textarea>
              </div>
              <div className="col-lg-4 col-sm-6 col-12 mb-2">
                SKHP
                <input type="text" className="form-control form-control-sm" onChange={(e) => changeSKHP(e.target.value, i)} />
              </div>
            </div>
          </div>
        </div>
      </div>
    ))}

    <div className="row mb-2">
      <div className="col-xl-8 col-lg-7"></div>
      <div className="col-xl-3 col-lg-4 text-end">
        <button onClick={kurangUTTP} className='btn btn-primary btn-sm'>
          <div>
            <i className="bi bi-dash-circle-fill"></i>
          </div>
          <div>
            Kurang UTTP
          </div>
        </button>
        <button onClick={tambahUTTP} className='btn btn-primary btn-sm ms-2 my-2'>
          <div>
            <i className="bi bi-plus-circle-fill"></i>
          </div>
          <div>
            Tambah UTTP
          </div>
        </button>
      </div>
      <div className="col-1"></div>
    </div>

    <div className="row">
      <div className="col-xl-8 col-lg-7"></div>
      <div className="col-xl-3 col-lg-4 text-end">
        <button onClick={submit} className='btn btn-primary'>
          Kirim
        </button>
      </div>
    </div>
  </div>
  )
}

export default PendaftaranTeraComponent
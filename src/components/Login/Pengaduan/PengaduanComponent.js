import React, { useState, useEffect } from 'react'
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import $ from 'jquery'
import url from '../../../helpers/url'

const PengaduanComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);

    const [alamat, setAlamat] = useState('');
    const [noHp, setNoHP] = useState('');
    const [uraian, setUraian] = useState('');
    const [bukti, setBukti] = useState([]);

    const history = useNavigate();
    const axiosJWT = axios.create();

    $(".pengaduan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });


    const submit = async () => {
        let reqPengaduan = {
            email: users.email,
            nama: users.nama,
            alamat: alamat,
            noHp: noHp,
            uraian: uraian
        }
        const response = await axios.post(`${url}/pengaduan`, reqPengaduan)

        const formData = new FormData();
        for (let i = 0; i < bukti.length; i++) {
            formData.append('files', bukti[i]);
        }
        await axios.post(`${url}/bukti-pengaduan/${response.data.id}`, formData)
        window.location.reload();
    }

    return (<div className="container">
        <div className="row">
            <div className="col-1"></div>
            <div className="col-10 text-center">
                <h4>FORMULIR PENGADUAN PELAYANAN TERA/TERA ULANG</h4>
            </div>
        </div>
        <br />
        <div className="row">
            <div className="col-1"></div>
            <div className="col-10">
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        Nama
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        {(() => {
                            if (users.length !== 0) {
                                return <input type="text" className="form-control form-control-sm" value={users.nama} disabled />
                            }
                        })()}
                    </div>
                </div>
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        Alamat
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        <textarea rows="4" className="form-control form-control-sm" onChange={(e) => setAlamat(e.target.value)}></textarea>
                    </div>
                </div>
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        No HP
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        <input type="text" className="form-control form-control-sm" onChange={(e) => setNoHP(e.target.value)} />
                    </div>
                </div>
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        Alamat Email
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        {(() => {
                            if (users.length !== 0) {
                                return <input type="email" className="form-control form-control-sm" value={users.email} disabled />
                            }
                        })()}
                    </div>
                </div>
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        Uraian Pengaduan
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        <textarea rows="8" className="form-control form-control-sm" onChange={(e) => setUraian(e.target.value)}></textarea>
                    </div>
                </div>
                <div className='row mb-2'>
                    <div className="col-lg-2 col-md-3 col-sm-3 col-3 title-sub d-flex align-items-center">
                        Bukti Pengaduan
                    </div>
                    <div className="col-lg-10 col-md-9 col-sm-9 col-9">
                        <input className="form-control" type="file" accept="image/*" onChange={(e) => setBukti(e.target.files)} multiple />
                    </div>
                </div>
            </div>
        </div>

        <div className="row">
            <div className="col-8"></div>
            <div className="col-3 text-end">
                <button onClick={submit} className='btn btn-primary'>
                    Kirim
                </button>
            </div>
        </div>

    </div>
    )
}

export default PengaduanComponent
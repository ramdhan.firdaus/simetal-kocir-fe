import React, { useEffect, useState } from 'react'
import $ from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../helpers/url';
import Lightbox from 'react-image-lightbox';

const TabelPengaduanComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);
    const history = useNavigate();
    const axiosJWT = axios.create();

    const [dataPengaduan, setDataPengaduan] = useState([])
    const [dataBukti, setDatabukti] = useState([[]])
    const [dataTanggal, setDataTanggal] = useState([])

    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    const [isOpen, setIsOpen] = useState(false)
    const [photoIndex, setPhotoIndex] = useState(0)
    const [images, setImages] = useState([])

    $(".pengaduan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            await axios.get(`${url}/pengaduan`).then(async (res) => {
                let dataBukti = []
                let dataTanggal = []
                const monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                await res.data.forEach(async (element, i) => {
                    await axios.get(`${url}/bukti-pengaduan/${element.id}`).then((response) => {
                        dataBukti.push(response.data)
                    })
                    let date = new Date(element.createdAt)
                    dataTanggal.push(`${date.getDay()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`)
                });
                setDatabukti(dataBukti)
                setDataTanggal(dataTanggal)
                setDataPengaduan(res.data)
            })
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    const openImage = (images) => {
        setPhotoIndex(0)
        setImages(images)
        setIsOpen(true)
    }

    return (<div className='container'>
        <div className="row mt-5 mb-2">
            <div className="col-2"></div>
            <div className="col-8 text-center">
                <h5><strong>PENGADUAN PELAYANAN KEMETROLOGIAN</strong></h5>
            </div>
        </div>
        <br></br>
        <div className="row">
            <div className="col-12">
                <table className="table table-bordered align-middle">
                    <thead className='text-center'>
                        <tr>
                            <th width="10px" scope="col">No</th>
                            <th width="100px" scope="col">Nama Pengadu</th>
                            <th width="100px" scope="col">Alamat</th>
                            <th width="100px" scope="col">No HP</th>
                            <th width="100px" scope="col">Email</th>
                            <th width="100px" scope="col">Tanggal Pengaduan</th>
                            <th width="100px" scope="col">Uraian Pengaduan</th>
                            <th width="100px" scope="col">Bukti Pengaduan</th>
                            <th width="150px" scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(() => {
                            if (dataPengaduan.length !== 0 && dataBukti.length !== 0) {
                                return <>
                                    {dataPengaduan.map((element, i) => {
                                        return <tr key={i}>
                                            <th scope="row" className='text-center'>{i + 1}</th>
                                            <td>{element.nama}</td>
                                            <td>{element.alamat}</td>
                                            <td>{element.noHP}</td>
                                            <td>{element.akunEmail}</td>
                                            <td>{dataTanggal[i]}</td>
                                            <td>{element.uraian}</td>
                                            <td>
                                                {(() => {
                                                    if (dataBukti[i]) {
                                                        return <>
                                                            {dataBukti[i].map((element, j) => (
                                                                <div key={j}><strong>{j + 1}.</strong> {element.bukti}</div>
                                                            ))}
                                                        </>
                                                    }
                                                })()}
                                            </td>
                                            <td className='text-center'>
                                                {(() => {
                                                    if (users.length !== 0) {
                                                        return <>
                                                            {(() => {
                                                                if (users.role.nama === "KEPALA DINAS" || users.role.nama === "SEKRETARIS DINAS"
                                                                    || users.role.nama === "KEPALA BIDANG" || users.role.nama === "SUB KOORDINATOR"
                                                                    || users.role.nama === "ADMIN" || users.role.nama === "PENGAWAS"
                                                                    || users.role.nama === "PENGGUNA") {
                                                                    return <>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => openImage(dataBukti[i])}>VIEW</button>
                                                                        <button className='btn btn-primary btn-sm mx-1 my-1'>BERI TANGGAPAN</button>
                                                                    </>
                                                                }
                                                            })()}
                                                        </>
                                                    }
                                                })()}
                                            </td>
                                        </tr>
                                    })}
                                </>
                            }
                        })()}
                    </tbody>
                </table>
            </div>
        </div>
        {isOpen && (
            <Lightbox
                mainSrc={`${url}/${images[photoIndex].bukti}`}
                nextSrc={images[(photoIndex + 1) % images.length].bukti}
                prevSrc={images[(photoIndex + images.length - 1) % images.length].bukti}
                onCloseRequest={() => setIsOpen(false)}
                onMovePrevRequest={() =>
                    setPhotoIndex((photoIndex + images.length - 1) % images.length)
                }
                onMoveNextRequest={() =>
                    setPhotoIndex((photoIndex + images.length + 1) % images.length)
                }
            />
        )}
    </div>
    )
}

export default TabelPengaduanComponent
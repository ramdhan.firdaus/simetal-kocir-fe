import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import url from '../../../helpers/url'
import Select from 'react-select';
import $ from 'jquery'

const EditSPTComponent = () => {
    const options = [
        { value: 'Tera', label: 'Tera' },
        { value: 'Tera Ulang', label: 'Tera Ulang' }
    ];

    const { no } = useParams()
    const [tera, setTera] = useState({})
    const [uttp, setUttp] = useState([])
    const [spt, setSpt] = useState({})
    const [statusSpt, setStatusSpt] = useState('')
    const [petugas, setPetugas] = useState([])
    const [revisi, setRevisi] = useState([])

    const [daftarKecamatan, setDaftarKecamatan] = useState([])
    const [daftarKelurahan, setDaftarKelurahan] = useState([])
    const [daftarjenisUTTP, setDaftarJenisUTTP] = useState([])
    const [daftarSatuanKapasitas, setDaftarSatuanKapasitas] = useState([]);
    const [daftarPetugas, setDaftarPetugas] = useState([])
    const [tambahPetugas, setTambahPetugas] = useState([])
    const [petugasTambahan, setPetugasTambahan] = useState([])
    const [hapusPetugas, setHapusPetugas] = useState([])

    const [errorKelurahan, setErrorKelurahan] = useState(false)
    const [errorPetugas, setErrorPetugas] = useState('')

    $(".penerbitan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            axios.get(`${url}/pendaftaran-tera/${no}`).then((res) => {
                setTera(res.data)

                axios.get(`${url}/kelurahan/${res.data.kelurahan.kecamatanId}`).then(async (res) => {
                    let data = []
                    await res.data.forEach((element) => {
                        data.push({ value: `${element.id}`, label: `${element.nama}` })
                    })
                    setDaftarKelurahan(data)
                })
            })

            axios.get(`${url}/uttp/${no}`).then((res) => {
                setUttp(res.data)
            })

            axios.get(`${url}/spt/${no}`).then((res) => {
                setStatusSpt(res.data.status.substring(0, 1).toUpperCase() + res.data.status.substring(1).toLowerCase())
                setSpt(res.data)
            })

            axios.get(`${url}/petugas-spt/${no}`).then((res) => {
                setPetugas(res.data)
            })

            axios.get(`${url}/kecamatan`).then(async (res) => {
                let data = []
                await res.data.forEach((element) => {
                    data.push({ value: `${element.id}`, label: `${element.nama}` })
                })
                setDaftarKecamatan(data)
            })

            axios.get(`${url}/sub-jenis-uttp`).then(async (res) => {
                let data = []
                await res.data.forEach((element) => {
                    data.push({ value: `${element.id}`, label: `${element.jenis}` })
                })
                setDaftarJenisUTTP(data)
            })

            axios.get(`${url}/satuan-kapasitas`).then(async (res) => {
                let data = []
                await res.data.forEach((element) => {
                    data.push({ value: `${element.id}`, label: `${element.nama}` })
                })
                setDaftarSatuanKapasitas(data)
            })

            axios.get(`${url}/akun/petugas`).then((res) => {
                let dataPetugas = []
                res.data.forEach(element => {
                    dataPetugas.push({ value: `${element.nip} - ${element.nama}`, label: `${element.nip} - ${element.nama}` })
                });
                setDaftarPetugas(dataPetugas)
            })
            axios.get(`${url}/revisi/${no}`).then((res) => {
                setRevisi(res.data)
            })
        }

        getApi()
    }, []);

    const changeKecamatan = (value, label) => {
        const updateTera = { ...tera }
        updateTera.kelurahan.kecamatan.nama = label
        updateTera.kelurahan.kecamatanId = value
        setTera(updateTera)

        const getApi = async () => {
            let data = []
            await axios.get(`${url}/kelurahan/${value}`).then((res) => {
                res.data.forEach((element) => {
                    data.push({ value: `${element.id}`, label: `${element.nama}` })
                })
            })

            setDaftarKelurahan(data)
            setErrorKelurahan(true)
        }

        getApi()
    }

    const changeKelurahan = (value, label) => {
        const updateTera = { ...tera }
        updateTera.kelurahan.nama = label
        updateTera.kelurahanId = value
        setTera(updateTera)
        setErrorKelurahan(false)
    }

    const changeTera = (value, column) => {
        const updateTera = { ...tera }
        updateTera[column] = value
        setTera(updateTera)
    }

    const changePetugas = async (i, value) => {
        const array = value.split(" - ")
        if (array[0] === '') setErrorPetugas('')
        else {
            let checked = false
            for (const [j, element] of petugas.entries()) {
                if (i === j) continue
                if (element !== undefined && element.nip === (array[0])) {
                    setErrorPetugas(`Petugas dengan nip ${array[0]} sudah terdaftar. Harap Cek kembali`)
                    checked = true
                    break
                }
            }
            if (!checked) {
                petugas[i].nip = array[0]
                petugas[i].nama = array[1]
                setPetugas(petugas)
                setErrorPetugas('')
            }
        }
    }

    const changePetugasTambahan = async (i, value) => {
        const array = value.split(" - ")
        if (array[0] === '') setErrorPetugas('')
        else {
            let checked = false
            for (const [j, element] of petugas.entries()) {
                if (element !== undefined && element.nip === (array[0])) {
                    setErrorPetugas(`Petugas dengan nip ${array[0]} sudah terdaftar. Harap Cek kembali`)
                    checked = true
                    break
                }
            }
            for (const [j, element] of tambahPetugas.entries()) {
                if (i === j) continue
                if (element !== undefined && element.nip === (array[0])) {
                    setErrorPetugas(`Petugas dengan nip ${array[0]} sudah terdaftar. Harap Cek kembali`)
                    checked = true
                    break
                }
            }
            if (!checked) {
                const updateTambahPetugas = [...tambahPetugas]
                let data = {
                    nip: array[0],
                    nama: array[1]
                }
                updateTambahPetugas[i] = data
                setTambahPetugas(updateTambahPetugas)
                setErrorPetugas('')
            }
        }
    }

    const deletePetugasByElement = (i, element) => {
        if (petugas.length !== 1) {
            const updateHapusPetugas = [...hapusPetugas]
            updateHapusPetugas.push(element)
            setHapusPetugas(updateHapusPetugas)

            const updatePetugas = [...petugas]
            delete updatePetugas[i]
            setPetugas(updatePetugas)
        }
    }

    const deletePetugasByIndex = (i) => {
        const updateTambahPetugas = [...tambahPetugas]
        delete updateTambahPetugas[i]
        setTambahPetugas(updateTambahPetugas)
        setPetugasTambahan(new Array(petugasTambahan.length - 1).fill(''))
    }

    const changeSPT = (value, column) => {
        const updateSPT = { ...spt }
        updateSPT[column] = value
        setSpt(updateSPT)
    }

    const changeUTTP = (i, value, column) => {
        const updateUTTP = [...uttp]
        updateUTTP[i][column] = value
        setUttp(updateUTTP)
    }

    const changeSelectUTTP = (i, value, label, column1, column21, column22) => {
        const updateUTTP = [...uttp]
        updateUTTP[i][column1] = value
        updateUTTP[i][column21][column22] = label
        setUttp(updateUTTP)
    }

    const saveTera = () => {
        let dataTera = {
            namaPemilik: tera.namaPemilik,
            alamatLengkap: tera.alamatLengkap,
            kelurahanId: tera.kelurahanId,
            jenisUsaha: tera.jenisUsaha,
            noHP: tera.noHP
        }
        axios.put(`${url}/pendaftaran-tera/${no}`, dataTera)
    }

    const saveSPT = async () => {
        let dataSPT = {
            pemohon: spt.pemohon,
            tanggalPelaksanaan: spt.tanggalPelaksanaan,
            alamat: spt.alamat
        }
        await axios.put(`${url}/spt/${no}`, dataSPT)

        let dataPetugas = {
            petugas: petugas
        }
        await axios.put(`${url}/petugas-spt`, dataPetugas)

        let dataTambahPetugas = {
            petugas: tambahPetugas
        }
        await axios.post(`${url}/petugas-spt/${no}`, dataTambahPetugas)

        await axios.delete(`${url}/petugas-spt/`, { data: { petugas: hapusPetugas } })
    }

    const saveUTTP = () => {
        let dataUTTP = {
            data: uttp
        }
        axios.put(`${url}/uttp/${no}`, dataUTTP)
    }

    const revisiSelesai = (i, id) => {
        const updateRevisi = [...revisi]
        updateRevisi[i].status = 'SELESAI'
        setRevisi(updateRevisi)
        axios.put(`${url}/revisi/${id}`)
    }

    return (<div className='container'>
        <div className='row'>
            <div className='col-12'>
                <div className="col-2 mb-2">
                    <a href='/simetal-kocir/penerbitan-spt'><button type='button' className='btn btn-primary'>Back</button></a>
                </div>
                <section className="content mb-3">
                    <div className="card card-widget widget-user">
                        <div className='mx-4 my-3'>
                            <h4>Daftar Revisi</h4>
                            <hr></hr>
                            {(() => {
                                if (revisi.length !== 0) {
                                    return <>
                                        {revisi.map((element, i) => (
                                            <div className='row' key={i}>
                                                <h6>Revisi ke-{i + 1}</h6>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Revisi</span>
                                                            <textarea rows={1} type="text" className="form-control form-control-sm" disabled value={element.detail} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Perevisi</span>
                                                            <input type="text" className="form-control form-control-sm" disabled value={`${element.rolePerevisi} - ${element.namaPerevisi}`} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-lg-3 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Status</span>
                                                            <input type="text" className="form-control form-control-sm" disabled value={element.status} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-1'>
                                                    <div className="mb-3">
                                                        {(() => {
                                                            if (element.status === 'SELESAI') {
                                                                return <button className='btn btn-primary btn-sm' disabled>Selesai</button>
                                                            } else {
                                                                return <button className='btn btn-primary btn-sm' onClick={() => revisiSelesai(i, element.id)}>Selesai</button>
                                                            }
                                                        })()}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </>
                                }
                            })()}
                        </div>
                    </div>
                </section>
            </div>
            <div className='col-lg-6 col-md-12'>
                {(() => {
                    if (tera.noOrder !== undefined) {
                        return <section className="content mb-3">
                            <div className="card card-widget widget-user">
                                <div className='mx-4 my-3'>
                                    <h4>Informasi Tera</h4>
                                    <hr></hr>
                                    <div className='row'>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Tanggal Pendaftaran Tera</label>
                                                <input type="date" className="form-control" id="exampleFormControlInput1"
                                                    value={tera.tanggal.substring(0, 10)} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'></div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">No Order</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1" value={tera.noOrder} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Nama Pemilik UTTP</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={tera.namaPemilik} onChange={(e) => changeTera(e.target.value, "namaPemilik")} />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Alamat Lengkap</label>
                                                <textarea type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={tera.alamatLengkap} onChange={(e) => changeTera(e.target.value, "alamatLengkap")} />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Kecamatan</label>
                                                <Select
                                                    value={daftarKecamatan.value}
                                                    options={daftarKecamatan}
                                                    defaultValue={{ value: `${tera.kelurahan.kecamatanId}`, label: `${tera.kelurahan.kecamatan.nama}` }}
                                                    onChange={(e) => changeKecamatan(e.value, e.label)}
                                                />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Kelurahan</label>
                                                <Select
                                                    value={daftarKelurahan.value}
                                                    options={daftarKelurahan}
                                                    defaultValue={{ value: `${tera.kelurahanId}`, label: `${tera.kelurahan.nama}` }}
                                                    onChange={(e) => changeKelurahan(e.value, e.label)}
                                                />
                                                {(() => {
                                                    if (errorKelurahan) {
                                                        return <p style={{ color: "red" }}>Harap ubah juga Kelurahannya</p>
                                                    }
                                                })()}
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Jenis Usaha</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={tera.jenisUsaha} onChange={(e) => changeTera(e.target.value, "jenisUsaha")} />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">No HP</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={tera.noHP} onChange={(e) => changeTera(e.target.value, "jenisUsaha")} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='text-center'>
                                        <button className='btn btn-primary mx-1 my-1' onClick={saveTera}>Save Tera</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    }
                })()}
            </div>
            <div className='col-lg-6 col-md-12'>
                {(() => {
                    if (spt.status !== undefined) {
                        return <section className="content mb-3">
                            <div className="card card-widget widget-user">
                                <div className='mx-4 my-3'>
                                    <h4>Informasi Surat Perintah Tugas</h4>
                                    <hr></hr>
                                    <div className='row'>
                                        <div className='col-8'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Status SPT</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1" value={statusSpt} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Nama Pemohon</label>
                                                <input type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={spt.pemohon} onChange={(e) => changeSPT(e.target.value, "pemohon")} />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Tanggal Pelaksanaan</label>
                                                <input type="date" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={spt.tanggalPelaksanaan.substring(0, 10)} onChange={(e) => changeSPT(e.target.value, "tanggalPelaksanaan")} />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Alamat Pelaksanaan</label>
                                                <textarea type="text" className="form-control" id="exampleFormControlInput1"
                                                    defaultValue={spt.alamat} onChange={(e) => changeSPT(e.target.value, "alamat")} />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <label htmlFor="exampleFormControlInput1" className="form-label">
                                                Daftar Petugas
                                            </label>
                                            {petugas.map((element, i) => (
                                                <div className='row' key={i}>
                                                    <div className='col-10'>
                                                        <Select
                                                            value={daftarPetugas.value}
                                                            options={daftarPetugas}
                                                            defaultValue={{ value: `${element.nip} - ${element.nama}`, label: `${element.nip} - ${element.nama}` }}
                                                            onChange={(e) => changePetugas(i, e.value)}
                                                            className='mb-2'
                                                        />
                                                    </div>
                                                    <div className='col-1'>
                                                        <div className='text-start'>
                                                            <button className='btn btn-danger btn-sm' onClick={() => deletePetugasByElement(i, element)}><i className="bi bi-trash3"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                            {petugasTambahan.map((element, i) => (
                                                <div className='row' key={`tambahan${i}`}>
                                                    <div className='col-10'>
                                                        <Select
                                                            value={daftarPetugas.value}
                                                            options={daftarPetugas}
                                                            defaultValue={{ value: '', label: 'Pilih Petugas' }}
                                                            onChange={(e) => changePetugasTambahan(i, e.value)}
                                                            className='mb-2'
                                                        />
                                                    </div>
                                                    <div className='col-1'>
                                                        <div className='text-start'>
                                                            <button className='btn btn-danger btn-sm' onClick={() => deletePetugasByIndex(i)}><i className="bi bi-trash3"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                            {(() => {
                                                if (errorPetugas !== '') {
                                                    return <p style={{ color: "red" }}>{errorPetugas}</p>
                                                }
                                            })()}
                                            <div className='text-start'>
                                                <button className='btn btn-primary mx-1 my-1' onClick={() => setPetugasTambahan(new Array(petugasTambahan.length + 1).fill(''))}>Tambah Petugas</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='text-center'>
                                        <button className='btn btn-primary mx-1 my-1' onClick={saveSPT}>Save SPT</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    }
                })()}
            </div>
            <div className='col-12'>
                <section className="content mb-3">
                    <div className="card card-widget widget-user">
                        <div className='mx-4 my-3'>
                            <h4>Informasi UTTP</h4>
                            {uttp.map((element, i) => (
                                <div key={i}>
                                    {(() => {
                                        if (element.jenisPelayanan !== undefined) {
                                            return <div className='row'>
                                                <hr></hr>
                                                <h6 className='mb-2'>UTTP ke-{i + 1}</h6>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Jenis Pelayanan</label>
                                                        <input type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.jenisPelayanan} onChange={(e) => changeUTTP(i, e.target.value, "jenisPelayanan")} />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Tera/Tera Ulang</label>
                                                        <Select
                                                            value={options.value}
                                                            options={options}
                                                            defaultValue={{ value: `${element.teraOrTeraUlang}`, label: `${element.teraOrTeraUlang}` }}
                                                            onChange={(e) => changeUTTP(i, e.value, "teraOrTeraUlang")}
                                                        />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Jenis UTTP</label>
                                                        <Select
                                                            value={daftarjenisUTTP.value}
                                                            options={daftarjenisUTTP}
                                                            defaultValue={{ value: `${element.jenisUttpId}`, label: `${element.sub_jenis_uttp.jenis}` }}
                                                            onChange={(e) => changeSelectUTTP(i, e.value, e.label, "subJenisUttpId", "sub_jenis_uttp", "jenis")}
                                                        />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Kapasitas</label>
                                                        <div className="input-group">
                                                            <input type="number" className="form-control nilai" id="exampleFormControlInput1" defaultValue={`${element.kapasitas}`} />
                                                            <Select
                                                                value={daftarSatuanKapasitas.value}
                                                                options={daftarSatuanKapasitas}
                                                                defaultValue={{ value: `${element.satuanKapasitaId}`, label: `${element.satuan_kapasita.nama}` }}
                                                                onChange={(e) => changeSelectUTTP(i, e.value, e.label, "satuanKapasitaId", "satuan_kapasita", "nama")}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Merek</label>
                                                        <input type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.merek} onChange={(e) => changeUTTP(i, e.target.value, "merek")} />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Model</label>
                                                        <input type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.model} onChange={(e) => changeUTTP(i, e.target.value, "model")} />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">No Seri</label>
                                                        <input type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.noSeri} onChange={(e) => changeUTTP(i, e.target.value, "noSeri")} />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Kelengkapan UTTP</label>
                                                        <textarea type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.kelengkapanUTTP} onChange={(e) => changeUTTP(i, e.target.value, "kelengkapanUTTP")} />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">SKHP</label>
                                                        <input type="text" className="form-control" id="exampleFormControlInput1"
                                                            defaultValue={element.skhp} onChange={(e) => changeUTTP(i, e.target.value, "skhp")} />
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    })()}
                                </div>
                            ))}
                            <div className='text-center'>
                                <button className='btn btn-primary mx-1 my-1' onClick={saveUTTP}>Save UTTP</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    )
}

export default EditSPTComponent
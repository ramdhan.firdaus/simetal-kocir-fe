import React, { useEffect, useState } from 'react'
import $, { data } from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../helpers/url';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

const PenerbitanSPTComponent = () => {
  const [token, setToken] = useState('');
  const [expire, setExpire] = useState('');
  const [users, setUsers] = useState([]);
  const history = useNavigate();
  const axiosJWT = axios.create();

  const [dataSPT, setDataSPT] = useState([])
  const [petugas, setPetugas] = useState([[]])
  const [dataTanggal, setDataTanggal] = useState([])
  const [detail, setDetail] = useState('')
  const [show, setShow] = useState(false);
  const [varModal, setVarModal] = useState({})

  $(".penerbitan").addClass("active")

  useEffect(() => {
    const getApi = async () => {
      await axios.get(`${url}/spt`).then(async (res) => {
        let dataPetugas = []
        let dataTanggal = []
        const monthNames = ["January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];
        await res.data.forEach(async (element, i) => {
          await axios.get(`${url}/petugas-spt/${element.pendaftaranTeraNoOrder}`).then((response) => {
            dataPetugas.push(response.data)
          })
          let date = new Date(element.tanggalPelaksanaan)
          dataTanggal.push(`${date.getDay()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`)
          setPetugas(dataPetugas)
        });
        setDataTanggal(dataTanggal)
        setDataSPT(res.data)
      })
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      try {
        const response = await axios.get(`${url}/token`);
        setToken(response.data.accessToken);
        const decoded = jwt_decode(response.data.accessToken);
        setExpire(decoded.exp);
      } catch (error) {
        if (error.response) {
          history("/");
        }
      }
    }

    getApi()
  }, []);

  useEffect(() => {
    const getApi = async () => {
      await axiosJWT.get(`${url}/akun`, {
        headers: {
          Authorization: `Bearer ${token}`
        },
      }).then((res) => {
        setUsers(res.data[0]);
      });
    }

    getApi()
  }, [token]);

  axiosJWT.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expire * 1000 < currentDate.getTime()) {
      const response = await axios.get(`${url}/token`);
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decoded = jwt_decode(response.data.accessToken);
      setExpire(decoded.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  });

  const aksiSPT = async (element, aksi, i) => {
    let data = {
      aksi: aksi
    }

    await axios.put(`${url}/spt/aksi/${element.pendaftaranTeraNoOrder}`, data)

    element.status = aksi
    const updateSPT = [...dataSPT];
    updateSPT[i] = element;
    setDataSPT(updateSPT);
  }

  const review = (element) => {
    history(`./review/${element.pendaftaranTeraNoOrder}`)
  }

  const edit = (element) => {
    history(`./edit/${element.pendaftaranTeraNoOrder}`)
  }

  const openModal = (element, i) => {
    let data = {
      element: element,
      i: i
    }
    setVarModal(data)
    handleShow()
  }

  const closeModal = () => {
    setDetail('')
    setVarModal({})
    handleClose()
  }

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const revisi = async () => {
    aksiSPT(varModal.element, `${users.role.nama} MEMINTA REVISI`, varModal.i)

    let data = {
      no: varModal.element.pendaftaranTeraNoOrder,
      detail: detail,
      nama: users.nama,
      role: users.role.nama
    }

    await axios.post(`${url}/revisi`, data)
    setDetail('')
    setVarModal({})
    closeModal()
  }

  return (<div className='container'>

    <div>
      <Modal show={show} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Revisi</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Detail Revisi</Form.Label>
              <Form.Control as="textarea" rows={3} onChange={(e) => setDetail(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
          <Button variant="primary" onClick={revisi}>
            Kirim
          </Button>
        </Modal.Footer>
      </Modal>
    </div>

    <div className="row mb-2 mt-5">
      <div className="col-2"></div>
      <div className="col-8 text-center">
        <h5>LIST SURAT PERINTAH TUGAS PELAYANAN TERA/TERA ULANG</h5>
      </div>
      <div className="col-2 align-self-end text-end">
        <a href='penerbitan-spt/tambah-spt'><button type='button' className='btn btn-primary btn-sm'>Tambah SPT Baru</button></a>
      </div>
    </div>
    <br></br>
    <div className="row">
      <div className="col-12">
        <table className="table table-bordered align-middle">
          <thead className='text-center'>
            <tr>
              <th width="10px" scope="col">No</th>
              <th width="100px" scope="col">Pemohon</th>
              <th width="100px" scope="col">Alamat</th>
              <th width="100px" scope="col">Tanggal Pelaksanaan Tugas</th>
              <th width="100px" scope="col">Petugas</th>
              <th width="100px" scope="col">Status</th>
              <th width="200px" scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataSPT.map((element, i) => {
              return <tr key={i}>
                <th scope="row" className='text-center'>{i + 1}</th>
                <td>{element.pemohon}</td>
                <td>{element.alamat}</td>
                <td>{dataTanggal[i]}</td>
                <td>
                  {(() => {
                    if (petugas[i]) {
                      return <>
                        {petugas[i].map((elementPetugas, j) => {
                          return <section key={j}><strong>{j + 1}.</strong> {elementPetugas.nama}</section>
                        })}
                      </>
                    }
                  })()}
                </td>
                <td>{element.status.slice(0, 1) + element.status.substr(1).toLowerCase()}</td>
                <td className='text-center'>

                  {/* REVIEW */}
                  <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => review(element)}>REVIEW</button>

                  {(() => {
                    if (users.length !== 0) {
                      return <>

                        {/* EDIT */}
                        {(() => {
                          if (users.role.nama === "ADMIN") {
                            if (element.status === 'BELUM DIVERIFIKASI' || element.status.substr(-14, 14) === 'MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => edit(element)}>EDIT</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>EDIT</button>
                            }
                          }
                        })()}

                        {/* REVISI */}
                        {(() => {
                          if (users.role.nama === "SUB KOORDINATOR") {
                            if (element.status === 'BELUM DIVERIFIKASI' || element.status === 'SUB KOORDINATOR MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => openModal(element, i)}>REVISI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>REVISI</button>
                            }
                          }
                        })()}
                        {(() => {
                          if (users.role.nama === "KEPALA BIDANG") {
                            if (element.status === 'SUDAH DIVERIFIKASI SUB KOORDINATOR' || element.status === 'KEPALA BIDANG MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => openModal(element, i)}>REVISI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>REVISI</button>
                            }
                          }
                        })()}
                        {(() => {
                          if (users.role.nama === "SEKRETARIS DINAS") {
                            if (element.status === 'SUDAH DIVERIFIKASI KEPALA BIDANG' || element.status === 'SEKRETARIS DINAS MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => openModal(element, i)}>REVISI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>REVISI</button>
                            }
                          }
                        })()}
                        {(() => {
                          if (users.role.nama === "KEPALA DINAS") {
                            if (element.status === 'SUDAH DIVERIFIKASI SEKRETARIS DINAS' || element.status === 'KEPALA DINAS MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => openModal(element, i)}>REVISI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>REVISI</button>
                            }
                          }
                        })()}

                        {/* VERIFIKASI */}
                        {(() => {
                          if (users.role.nama === "SUB KOORDINATOR") {
                            if (element.status === 'BELUM DIVERIFIKASI' || element.status === 'SUB KOORDINATOR MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => aksiSPT(element, `SUDAH DIVERIFIKASI ${users.role.nama}`, i)}>VERIFIKASI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>VERIFIKASI</button>
                            }
                          }
                        })()}
                        {(() => {
                          if (users.role.nama === "KEPALA BIDANG") {
                            if (element.status === 'SUDAH DIVERIFIKASI SUB KOORDINATOR' || element.status === 'KEPALA BIDANG MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => aksiSPT(element, `SUDAH DIVERIFIKASI ${users.role.nama}`, i)}>VERIFIKASI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>VERIFIKASI</button>
                            }
                          }
                        })()}
                        {(() => {
                          if (users.role.nama === "SEKRETARIS DINAS") {
                            if (element.status === 'SUDAH DIVERIFIKASI KEPALA BIDANG' || element.status === 'SEKRETARIS DINAS MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => aksiSPT(element, `SUDAH DIVERIFIKASI ${users.role.nama}`, i)}>VERIFIKASI</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>VERIFIKASI</button>
                            }
                          }
                        })()}

                        {/* TANDA TANGAN */}
                        {(() => {
                          if (users.role.nama === "KEPALA DINAS") {
                            if (element.status === 'SUDAH DIVERIFIKASI SEKRETARIS DINAS' || element.status === 'KEPALA DINAS MEMINTA REVISI') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' onClick={() => aksiSPT(element, `SUDAH DITANDATANGANI ${users.role.nama}`, i)}>TANDA TANGAN</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>TANDA TANGAN</button>
                            }
                          }
                        })()}

                        {/* DOWNLOAD */}
                        {(() => {
                          if (users.role.nama === "ADMIN" || users.role.nama === "PENERA") {
                            if (element.status === 'SUDAH DITANDATANGANI KEPALA DINAS') {
                              return <button className='btn btn-primary btn-sm mx-1 my-1'>DOWNLOAD</button>
                            } else {
                              return <button className='btn btn-primary btn-sm mx-1 my-1' disabled>DOWNLOAD</button>
                            }
                          }
                        })()}
                      </>
                    }
                  })()}
                </td>
              </tr>
            })}
          </tbody>
        </table>
      </div>
    </div>
  </div>
  )
}

export default PenerbitanSPTComponent
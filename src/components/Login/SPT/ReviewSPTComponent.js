import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import url from '../../../helpers/url'
import $ from 'jquery'

const ReviewSPTComponent = () => {
    const { no } = useParams()
    const [tera, setTera] = useState({})
    const [uttp, setUttp] = useState([])
    const [spt, setSpt] = useState({})
    const [statusSpt, setStatusSpt] = useState('')
    const [petugas, setPetugas] = useState([])
    const [revisi, setRevisi] = useState([])

    $(".penerbitan").addClass("active")

    useEffect(() => {
        const getApi = async () => {
            axios.get(`${url}/pendaftaran-tera/${no}`).then((res) => {
                setTera(res.data)
            })
            axios.get(`${url}/uttp/${no}`).then((res) => {
                setUttp(res.data)
            })
            axios.get(`${url}/spt/${no}`).then((res) => {
                setStatusSpt(res.data.status.substring(0, 1).toUpperCase() + res.data.status.substring(1).toLowerCase())
                setSpt(res.data)
            })
            axios.get(`${url}/petugas-spt/${no}`).then((res) => {
                setPetugas(res.data)
            })
            axios.get(`${url}/revisi/${no}`).then((res) => {
                console.log(res.data)
                setRevisi(res.data)
            })
        }

        getApi()
    }, []);

    return (<div className='container'>
        <div className='row'>
            <div className="col-2 mb-2">
                <a href='/simetal-kocir/penerbitan-spt'><button type='button' className='btn btn-primary btn-sm'>Back</button></a>
            </div>
            <div className='col-12'>
                <section className="content mb-3">
                    <div className="card card-widget widget-user">
                        <div className='mx-4 my-3'>
                            <h4>Daftar Revisi</h4>
                            <hr></hr>
                            {(() => {
                                if (revisi.length !== 0) {
                                    return <>
                                        {revisi.map((element, i) => (
                                            <div className='row' key={i}>
                                                <h6>Revisi ke-{i + 1}</h6>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Revisi</span>
                                                            <textarea rows={1} type="text" className="form-control form-control-sm" disabled value={element.detail} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Perevisi</span>
                                                            <input type="text" className="form-control form-control-sm" disabled value={`${element.rolePerevisi} - ${element.namaPerevisi}`} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <div className="input-group input-group-sm mb-3">
                                                            <span className="input-group-text" id="inputGroup-sizing-sm">Status</span>
                                                            <input type="text" className="form-control form-control-sm" disabled value={element.status} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </>
                                }
                            })()}
                        </div>
                    </div>
                </section>
            </div>
            <div className='col-lg-6 col-md-12'>
                {(() => {
                    if (tera.noOrder !== undefined) {
                        return <section className="content mb-3">
                            <div className="card card-widget widget-user">
                                <div className='mx-4 my-3'>
                                    <h4>Informasi Tera</h4>
                                    <hr></hr>
                                    <div className='row'>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Tanggal Pendaftaran Tera</label>
                                                <input type="date" className="form-control form-control-sm" id="exampleFormControlInput1"
                                                    value={tera.tanggal.substring(0, 10)} disabled
                                                />
                                            </div>
                                        </div>
                                        <div className='col-6'></div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">No Order</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.noOrder} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Nama Pemilik UTTP</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.namaPemilik} disabled />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Alamat Lengkap</label>
                                                <textarea type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.alamatLengkap} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Kecamatan</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.kelurahan.kecamatan.nama} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Kelurahan</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.kelurahan.nama} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Jenis Usaha</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.jenisUsaha} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">No HP</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={tera.noHP} disabled />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    }
                })()}
            </div>
            <div className='col-lg-6 col-md-12'>
                {(() => {
                    if (spt.status !== undefined) {
                        return <section className="content mb-3">
                            <div className="card card-widget widget-user">
                                <div className='mx-4 my-3'>
                                    <h4>Informasi Surat Perintah Tugas</h4>
                                    <hr></hr>
                                    <div className='row'>
                                        <div className='col-8'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Status SPT</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={statusSpt} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Nama Pemohon</label>
                                                <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={spt.pemohon} disabled />
                                            </div>
                                        </div>
                                        <div className='col-6'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Tanggal Pelaksanaan</label>
                                                <input type="date" className="form-control form-control-sm" id="exampleFormControlInput1" value={spt.tanggalPelaksanaan.substring(0, 10)} disabled />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className="mb-3">
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Alamat Pelaksanaan</label>
                                                <textarea type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={spt.alamat} disabled />
                                            </div>
                                        </div>
                                        <div className='col-12'>
                                            <div className='row'>
                                                <label htmlFor="exampleFormControlInput1" className="form-label">Daftar Petugas</label>
                                                {petugas.map((element, i) => (
                                                    <div className='col-12 mb-2' key={i}>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={`${element.nip} - ${element.nama}`} disabled />
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    }
                })()}
            </div>
            <div className='col-12'>
                <section className="content mb-3">
                    <div className="card card-widget widget-user">
                        <div className='mx-4 my-3'>
                            <h4>Informasi UTTP</h4>
                            {uttp.map((element, i) => (
                                <div key={i}>
                                    {(() => {
                                        if (element.jenisPelayanan !== undefined) {
                                            return <div className='row'>
                                                <hr></hr>
                                                <h6 className='mb-2'>UTTP ke-{i + 1}</h6>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Jenis Pelayanan</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.jenisPelayanan} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Tera/Tera Ulang</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.teraOrTeraUlang} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Jenis UTTP</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.sub_jenis_uttp.jenis} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Kapasitas</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={`${element.kapasitas} ${element.satuan_kapasita.nama}`} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Merek</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.merek} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Model</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.model} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">No Seri</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.noSeri} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">Kelengkapan UTTP</label>
                                                        <textarea type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.kelengkapanUTTP} disabled />
                                                    </div>
                                                </div>
                                                <div className='col-lg-4 col-md-6'>
                                                    <div className="mb-3">
                                                        <label htmlFor="exampleFormControlInput1" className="form-label">SKHP</label>
                                                        <input type="text" className="form-control form-control-sm" id="exampleFormControlInput1" value={element.skhp} disabled />
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    })()}
                                </div>
                            ))}
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    )
}

export default ReviewSPTComponent
import React, { useEffect, useState } from 'react'
import $ from 'jquery'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../../helpers/url';
import Select from 'react-select';

const TambahSPTComponent = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);
    const history = useNavigate();
    const axiosJWT = axios.create();

    const [dataOrder, setDataOrder] = useState([])
    const [noOrder, setNoOrder] = useState('')
    const [nama, setNama] = useState('')
    const [tanggal, setTanggal] = useState('')
    const [alamat, setAlamat] = useState('')
    const [petugas, setPetugas] = useState([])
    const [nipPetugas, setNipPetugas] = useState([])
    const [jumlahPetugasTambahan, setJumlahPetugasTammbahan] = useState(0)
    const [petugasTambahan, setPetugasTambahan] = useState([])
    const [daftarPetugas, setDaftarPetugas] = useState([])
    const [msg, setMsg] = useState('')

    $(".penerbitan").addClass("active")

    useEffect(() => {
        const getApiPetugas = async () => {
            let dataPetugas = []
            dataPetugas.push({ value: '', label: 'Pilih Petugas' })
            await axios.get(`${url}/akun/petugas`).then((res) => {
                res.data.forEach(element => {
                    dataPetugas.push({ value: `${element.nip} - ${element.nama}`, label: `${element.nip} - ${element.nama}` })
                });
            })
            setDaftarPetugas(dataPetugas)
        }

        const getApiNoOrder = async () => {
            let dataPendaftaran = []
            dataPendaftaran.push({ value: '--', label: 'Pilih No Order' })
            await axios.get(`${url}/pendaftaran-tera/not-spt`).then((res) => {
                res.data.forEach(element => {
                    dataPendaftaran.push({ value: `${element.noOrder}:${element.namaPemilik}:${element.alamatLengkap}`, label: `${element.noOrder}` })
                });
            })
            setDataOrder(dataPendaftaran)
        }

        getApiPetugas()
        getApiNoOrder()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });


    const kurangPetugas = () => {
        if (jumlahPetugasTambahan !== 0) {
            setPetugasTambahan(new Array(jumlahPetugasTambahan - 1).fill(''))
            setJumlahPetugasTammbahan(jumlahPetugasTambahan - 1)
        }
    }

    const tambahPetugas = () => {
        setPetugasTambahan(new Array(jumlahPetugasTambahan + 1).fill(''))
        setJumlahPetugasTammbahan(jumlahPetugasTambahan + 1)
    }

    const changeNoOrder = (value) => {
        const array = value.split(':');
        setNoOrder(array[0])
        setNama(array[1])
        setAlamat(array[2])
    }

    const addPetugas = (value, i) => {
        const array = value.split(" - ")
        if (!nipPetugas.includes(array[0])) {
            const dataNip = [...nipPetugas]
            dataNip[i] = array[0]
            setNipPetugas(dataNip)

            const updatePetugas = [...petugas]
            let data = {
                nip: array[0],
                nama: array[1]
            }
            updatePetugas[i] = data
            setPetugas(updatePetugas)

            setMsg('')
        } else if (array[0] !== '') {
            setMsg(`Petugas dengan nip ${array[0]} sudah terdaftar`)
        } else {
            setMsg('')
        }
    }

    const submit = async () => {
        let dataSPT = {
            noOrder: noOrder,
            pemohon: nama,
            tanggalPelaksanaan: tanggal,
            alamat: alamat
        }
        await axios.post(`${url}/spt`, dataSPT)

        let dataPetugas = {
            petugas: petugas
        }
        await axios.post(`${url}/petugas-spt/${noOrder}`, dataPetugas)

        history("/simetal-kocir/penerbitan-spt")
    }

    return (<div className="container">
        <div className="row">
            <div className="col-2"></div>
            <div className="col-8 text-center">
                <h5>
                    FORM PENGISIAN PEMBUATAN SURAT PERINTAH TUGAS PELAYANAN TERA/TERA ULANG
                </h5>
            </div>
        </div>
        <br />
        <div className="row mb-2">
            <div className="col-2"></div>
            <div className="col-2 d-flex align-items-center">
                No Order Pelayanan
            </div>
            <div className="col-3">
                <Select
                    value={dataOrder.value}
                    options={dataOrder}
                    defaultValue={{ value: `--`, label: `Pilih No Order` }}
                    onChange={(e) => changeNoOrder(e.value, e.label)}
                />
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-2"></div>
            <div className="col-2 d-flex align-items-center">
                Nama Pemohon / Perusahaan
            </div>
            <div className="col-6">
                <input type="text" className="form-control form-control-sm" value={nama} disabled />
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-2"></div>
            <div className="col-2 d-flex align-items-center">
                Tanggal Pelaksanaan
            </div>
            <div className="col-3">
                <input type="date" className="form-control form-control-sm" onChange={(e) => setTanggal(e.target.value)} />
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-2"></div>
            <div className="col-2 d-flex align-items-center">
                Alamat Pelaksanaan
            </div>
            <div className="col-6">
                <textarea rows="4" className="form-control form-control-sm" value={alamat} disabled></textarea>
            </div>
        </div>

        <div className="row mb-2">
            <div className="col-2"></div>
            <div className="col-2 d-flex align-items-center">
                Petugas yang Ditunjuk
            </div>
            <div className="col-6">
                <Select
                    value={daftarPetugas.value}
                    options={daftarPetugas}
                    defaultValue={{ value: ``, label: `Pilih Petugas` }}
                    onChange={(e) => addPetugas(e.value, 0)}
                />
            </div>
        </div>

        {petugasTambahan.map((element, i) => {
            return <div key={`petugas-${i}`} className="row mb-2">
                <div className="col-2"></div>
                <div className="col-2"></div>
                <div className="col-6">
                    <select className="form-select form-select-sm" onChange={(e) => addPetugas(e.target.value, i + 1)}>
                        {daftarPetugas.map((element, j) => {
                            return <option key={`daftar-${j}-${i}`} value={element.value} label={element.label}></option>
                        })}
                    </select>
                </div>
            </div>
        })}

        {(() => {
            if (msg !== '') {
                return <div className="row mb-2">
                    <div className="col-2"></div>
                    <div className="col-2"></div>
                    <div className="col-6">
                        <p className='message-error'>{msg}</p>
                    </div>
                </div>
            }
        })()}

        <div className="row mb-2">
            <div className="col-7"></div>
            <div className="col-3 text-end">
                <button onClick={kurangPetugas} className='btn btn-primary btn-sm'>
                    <div>
                        <i className="bi bi-dash-circle-fill"></i>
                    </div>
                    <div>
                        Kurang Petugas
                    </div>
                </button>
                <button onClick={tambahPetugas} className='btn btn-primary btn-sm ms-2 my-2'>
                    <div>
                        <i className="bi bi-plus-circle-fill"></i>
                    </div>
                    <div>
                        Tambah Petugas
                    </div>
                </button>
            </div>
        </div>

        <div className="row">
            <div className="col-7"></div>
            <div className="col-3 text-end">
                <button onClick={() => history(-1)} className='btn btn-primary me-2'>
                    Cancel
                </button>
                <button onClick={submit} className='btn btn-primary'>
                    Kirim
                </button>
            </div>
        </div>
    </div>
    )
}

export default TambahSPTComponent
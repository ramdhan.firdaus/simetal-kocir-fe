import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import url from '../../helpers/url';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

const NavbarLogin = () => {
    const [token, setToken] = useState('');
    const [expire, setExpire] = useState('');
    const [users, setUsers] = useState([]);
    const history = useNavigate();
    const axiosJWT = axios.create();

    useEffect(() => {
        const getApi = async () => {
            try {
                const response = await axios.get(`${url}/token`);
                setToken(response.data.accessToken);
                const decoded = jwt_decode(response.data.accessToken);
                setExpire(decoded.exp);
            } catch (error) {
                if (error.response) {
                    history("/");
                }
            }
        }

        getApi()
    }, []);

    useEffect(() => {
        const getApi = async () => {
            await axiosJWT.get(`${url}/akun`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            }).then((res) => {
                setUsers(res.data[0]);
            });
        }

        getApi()
    }, [token]);

    axiosJWT.interceptors.request.use(async (config) => {
        const currentDate = new Date();
        if (expire * 1000 < currentDate.getTime()) {
            const response = await axios.get(`${url}/token`);
            config.headers.Authorization = `Bearer ${response.data.accessToken}`;
            setToken(response.data.accessToken);
            const decoded = jwt_decode(response.data.accessToken);
            setExpire(decoded.exp);
        }
        return config;
    }, (error) => {
        return Promise.reject(error);
    });

    const submit = async () => {
        try {
            await axios.delete(url + '/logout');
            history("/");
        } catch (error) {
            console.log(error);
        }
    }

    return (<div className="container mb-3">
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/">
                    <a className="navbar-brand" href="/">
                        <img src="" alt="" width="30" height="24" />
                    </a>
                    SIMETAL KOCIR
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link className='beranda' href="/simetal-kocir">Beranda</Nav.Link>
                        {(() => {
                            if (users.length !== 0) {
                                return <>
                                    {(() => {
                                        if (users.role.nama === "PENGGUNA") {
                                            return <Nav.Link className='pengaduan' href="/simetal-kocir/pengaduan">Pengaduan</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "KEPALA DINAS" || users.role.nama === "SEKRETARIS DINAS"
                                            || users.role.nama === "KEPALA BIDANG" || users.role.nama === "SUB KOORDINATOR"
                                            || users.role.nama === "ADMIN" || users.role.nama === "PENGAWAS"
                                            || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link className='pengaduan' href="/simetal-kocir/tabel-pengaduan">Pengaduan</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "PENGGUNA") {
                                            return <Nav.Link className='pendaftaran' href="/simetal-kocir/pendaftaran-tera">Pendaftaran Tera/Tera Ulang</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "KEPALA DINAS" || users.role.nama === "SEKRETARIS DINAS"
                                            || users.role.nama === "KEPALA BIDANG" || users.role.nama === "SUB KOORDINATOR"
                                            || users.role.nama === "ADMIN" || users.role.nama === "PENERA"
                                            || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link className='penerbitan' href="/simetal-kocir/penerbitan-spt">Penerbitan SPT</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "ADMIN" || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link className='admin' href="/simetal-kocir/layanan-admin">Layanan Admin</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "ADMIN" || users.role.nama === "PENGGUNA") {
                                            return <NavDropdown title="Database" id="basic-nav-dropdown" className='database'>
                                                <NavDropdown.Item href="/simetal-kocir/database-pelayanan" className='pelayanan'>Database Pelayanan</NavDropdown.Item>
                                                <NavDropdown.Item href="/simetal-kocir/database-pengamatan" className='pengamatan'>Database Pengamatan</NavDropdown.Item>
                                                <NavDropdown.Item href="/simetal-kocir/database-pengawasan" className='pengawasan'>Database Pengawasan</NavDropdown.Item>
                                            </NavDropdown>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "PENERA" || users.role.nama === "BENDAHARA PENERIMAAN" || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link href="/simetal-kocir/database-pelayanan" className='pelayanan'>Database Pelayanan</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "PENGAMAT" || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link href="/simetal-kocir/database-pengamatan" className='pengamatan'>Database Pengamatan</Nav.Link>
                                        }
                                    })()}
                                    {(() => {
                                        if (users.role.nama === "PENGAWAS" || users.role.nama === "PENGGUNA") {
                                            return <Nav.Link href="/simetal-kocir/database-pengawasan" className='pengawasan'>Database Pengawasan</Nav.Link>
                                        }
                                    })()}
                                </>
                            }
                        })()}
                    </Nav>
                    <Form className="d-flex">
                        <button className="btn btn-primary btn-sm btn-block" type="submit" onClick={submit}>Keluar</button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </div >
    )
}

export default NavbarLogin
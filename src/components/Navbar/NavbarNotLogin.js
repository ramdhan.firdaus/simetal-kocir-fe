import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import url from '../../helpers/url';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const NavbarNotLogin = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [msg, setMsg] = useState('');
    const history = useNavigate();

    const submit = async (e) => {
        e.preventDefault()
        try {
            await axios.post(url + '/login', { username: username, password: password })
            history('/simetal-kocir')
        }
        catch (e) {
            if (e.response) {
                setMsg(e.response.data.msg);
            }
        }
    }
    return (<div className="container mb-3">
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/">
                    <a className="navbar-brand" href="/">
                        <img src="" alt="" width="30" height="24" />
                    </a>
                    SIMETAL KOCIR
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link className='beranda' href="/">Beranda</Nav.Link>
                        <Nav.Link className='pengaduan' href="/pengaduan">Pengaduan</Nav.Link>
                        <Nav.Link className='pendaftaran' href="/pendaftaran-tera">Pendaftaran Tera/Tera Ulang</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <div className="form-outline mx-2">
                            <input type="text" className="form-control" placeholder='Masukkan Username'
                                onChange={(e) => setUsername(e.target.value)} />
                        </div>
                        <div className="form-outline mx-2">
                            <input type="password" className="form-control" placeholder='Masukkan Password'
                                onChange={(e) => setPassword(e.target.value)} />
                        </div>
                        <button className="btn btn-primary btn-sm btn-block mx-2" type="submit" onClick={submit}>Masuk</button>
                        <button className="btn btn-primary btn-sm btn-block mx-2" type="submit" onClick={(e) => history("/register-akun")}>Register</button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        {(() => {
            if (msg !== '') {
                return <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    {msg}
                    <button type="button" className="btn-close" onClick={() => setMsg('')}></button>
                </div>
            }
        })()}
    </div>
    )
}

export default NavbarNotLogin
import React from 'react';
import { Outlet } from 'react-router';
import NavbarLogin from './NavbarLogin';

const WithNavLogin = () => {
  return (
    <>
      <NavbarLogin />
      <Outlet />
    </>
  );
};

export default WithNavLogin
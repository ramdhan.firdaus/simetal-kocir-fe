import React from 'react';
import { Outlet } from 'react-router';
import NavbarNotLogin from './NavbarNotLogin';

const WithNavNotLogin = () => {
  return (
    <>
      <NavbarNotLogin />
      <Outlet />
    </>
  );
};

export default WithNavNotLogin
import React, { useEffect } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import $ from 'jquery';

const HomeLoginComponent = () => {
  const history = useNavigate();

  useEffect(() => {
    $(".beranda").addClass("active")
  }, []);

  useEffect(() => {
    const getApi = async () => {
      try {
        await axios.get('http://localhost:5000/token');
        history('/simetal-kocir')
      } catch (e) {
        console.clear()
      }
    }

    getApi()
  }, []);

  return (<div className="container">
    <section id="select">
      <div className='mx-4 my-2'>
        <strong>PROFIL KEMETRILOGIAN</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong>SEBARAN UTTP DI KOTA CIREBON</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong>TARIF RETRIBUSI</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div className='mx-4 my-4'>
        <strong><i>Tracking</i> Order Pelayanan</strong>
        <div className="form-outline my-1 mx-4">
          <div className="input-group">
            <input type="email" className="form-control" />
            <button className="btn btn-primary btn-block mx-2" type="submit">Cari</button>
          </div>
        </div>
      </div>
      <br></br>
      <hr />
      <div className='mx-4 my-4'>
        <strong>LOKASI DAN KONTAK</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </section>
  </div>
  )
}

export default HomeLoginComponent
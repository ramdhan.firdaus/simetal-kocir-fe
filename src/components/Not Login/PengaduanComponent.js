import React, { useEffect } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import $ from 'jquery'

const PengaduanComponent = () => {
  const history = useNavigate();

  useEffect(() => {
    $(".pengaduan").addClass("active")
  }, []);

  useEffect(() => {
    const getApi = async () => {
      try {
        await axios.get('http://localhost:5000/token');
        history('/simetal-kocir/pendaftaran')
      } catch (e) {
        console.clear()
      }
    }

    getApi()
  }, []);

  return (<div className="container">
    <div className="alert alert-danger alert-dismissible fade show mx-4" role="alert">
      Harap login terlebih dahulu!
    </div>
  </div>
  )
}

export default PengaduanComponent